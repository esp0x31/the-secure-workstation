# zathura - SELinux confined

## About
Securely confine the zathura application to a *SELinux domain*.

## Features
- Read-only access to PDF files
- Restrict access to sensitive files (SSH and GPG keys)
- Deny network connections
- Prevent launching new shells
 
## Compilation and installation
```
sh zathura.sh
```

## Requirements
This module is developed and tested on Gentoo Hardened with OpenRC.
(*Profile: default/linux/amd64/17.1/hardened/selinux (stable)*)

The system is configured to run SELinux enforcing in **strict mode**. 

## Known issues
- Improve the man page

## Maintainer
esp0x31@fastmail.com

