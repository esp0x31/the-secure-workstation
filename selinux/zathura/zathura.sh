#!/bin/sh -e

DIRNAME=`dirname $0`
cd $DIRNAME
if [ `id -u` != 0 ]; then
echo 'You must be root to run this script'
exit 1
fi

echo "Building and Loading Policy"
set -x
make -f /usr/share/selinux/strict/include/Makefile zathura.pp || exit
/usr/sbin/semodule -i zathura.pp

# Generate a man page off the installed module
sepolicy manpage -p . -d zathura_t
# Fixing the file context on /usr/bin/zathura
/sbin/restorecon -F -R -v /usr/bin/zathura

